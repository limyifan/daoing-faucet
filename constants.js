
export const networkSettings = {
    56: {
        chainId: "0x38",
        chainName: "BSC Mainnet",
        nativeCurrency: {
            name: "Binance Coin",
            symbol: "BNB",
            decimals: 18
        },
        rpcUrls: ["https://bsc-dataseed.binance.org"],
        blockExplorerUrls: ["https://bscscan.com/"]
    },
    97: {
        chainId: "0x61",
        chainName: "BSC TestNet",
        nativeCurrency: {
            name: "Binance Coin",
            symbol: "BNB",
            decimals: 18
        },
        rpcUrls: ["https://data-seed-prebsc-1-s1.binance.org:8545/"],
        blockExplorerUrls: ["https://testnet.bscscan.com/"]
    },
    80001: {
        chainId: "0x13881",
        chainName: "Polygon TestNet",
        nativeCurrency: {
            name: "Matic",
            symbol: "matic",
            decimals: 18
        },
        rpcUrls: ["https://matic-mumbai.chainstacklabs.com/"],
        blockExplorerUrls: ["https://mumbai.polygonscan.com/"]
    },
    4: {
        chainId: "0x4",
        chainName: "Rinkeby testnet",
        nativeCurrency: {
            name: "Rinkeby Ether",
            symbol: "ETH",
            decimals: 18
        },
        rpcUrls: ["https://rinkeby.infura.io/v3/"],
        blockExplorerUrls: ["https://rinkeby.etherscan.io"]
    }
};

export const DAOTokenAddress="0xc6c053806cc943bf618efd25ac43887f6310e98c"
 export const BSCFaucetAddress = "0x380F80c6CC3A81A77797c0578f70970E173845C3"


