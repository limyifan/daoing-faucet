import Vue from 'vue'
import Web3 from "web3";
import Web3Modal from "web3modal";
import {
  DAOTokenAddress,
  BSCFaucetAddress
} from "@/constants";
import DaoToken from "@/abi/DaoToken.json";
import Faucet from "@/abi/Faucet.json";
import {store} from "../store"
import WalletConnectProvider from "@walletconnect/web3-provider";

const {networkSettings} = require("@/constants");
const providerOptions = {
    walletconnect: {
        package: WalletConnectProvider,
        options: {
            rpc: {
                97: networkSettings[97].rpcUrls[0],
            },
            network: 'binance',
            infuraId: "734b096f2f4046bc9df34620b2f42979",
        },
    },
};
Vue.prototype.$wallet = {
  web3: null,
  walletType: '',
  provider: undefined,
  eth: null,
  chainId: null,
  web3Modal: undefined,
  isListening: false,
  getWeb3() {
    if (!this.web3 || !this.web3.currentProvider) {
      if (!this.provider) {
        this.web3 = new Web3()
        this.web3.eth.transactionBlockTimeout = 10000
        const chainId = 97
        this.provider = new this.web3.providers.HttpProvider(networkSettings[chainId].rpcUrls[0])
        this.web3.setProvider(this.provider)
      } else {
        this.web3 = new Web3(this.provider)
      }
    }
    return this.web3
  },
  async getAddress() {
    if (store.getters['user/account'] !== "") {
      console.log("acc yes")
      return store.getters['user/account']
    } else {
      console.log("acc no")
      try {
        const res = await this.getWeb3().eth.requestAccounts()
        store.commit("user/SET_ACCOUNT", res[0])
        return res[0]
      } catch (e) {
        console.log(e)
        return null
      }
    }


  },
  async initWallet() {
    if (this.web3Modal) {
      this.web3Modal.clearCachedProvider();
    } else {
      this.web3Modal = new Web3Modal({
        cacheProvider: true, // optional
        providerOptions, // required
        disableInjectedProvider: false,
        // theme: "dark"
      });
    }
    if (localStorage.getItem("WEB3_CONNECT_CACHED_PROVIDER")) {
      this.onConnect(true)
    }
  },
  // 连接钱包
  async onConnect(hadSign) {
    try {
      this.provider = await this.web3Modal.connect();
    } catch (e) {
      console.log("Could not get a wallet connection", e);
      return;
    }

    // Subscribe to accounts change
    this.provider.on("accountsChanged", accounts => {
      this.fetchAccountData(accounts[0], this.chainId, false);
      console.log("accountsChanged", accounts)
    });

    //Subscribe to chainId change
    this.provider.on("chainChanged", chainId => {
      this.fetchAccountData(this.account, Number(chainId), false);
      store.commit("user/SET_NETWORK_ID", Number(chainId))
      console.log("chainChanged", chainId)
    });

    // Subscribe to networkId change
    //this is deprecated
    // this.provider.on("networkChanged", networkId => {
    //     this.fetchAccountData(this.account, networkId, false);
    //     store.commit("user/SET_NETWORK_ID", networkId)
    //     console.log("networkChanged", networkId)
    // });

    this.provider.on("disconnect", networkId => {
      store.commit("user/SET_ACCOUNT", "")
      localStorage.removeItem('account')
      console.log("disconnect", networkId)
      const event = new CustomEvent('disconnect', {
        detail: {
          time: new Date(),
        },
        bubbles: true,
        cancelable: true,
      })
      window.document.dispatchEvent(event)
    });
    await this.fetchAccountData(this.account, this.chainId, !hadSign);
  },
  switchChain(chainId) {
    const that = this

    return new Promise(async (resolve, reject) => {

      that
        .getWeb3()
        .currentProvider.request({
        method: 'wallet_addEthereumChain',
        params: [networkSettings[chainId]],
      })
        .then(() => {
          resolve(true)
        }).catch(e => {
        console.log(e)
        // this.switchChain(chainId)
      })
    })
  },
  async getChainId() {
    if (store.getters['user/networkId'] !== "") {
      return store.getters['user/networkId']
    } else {
      const chainId = await this.getWeb3().eth.getChainId()
      store.commit("user/SET_NETWORK_ID", Number(chainId))
      return chainId;
    }

  },
  isAccountExists(account) {
    if (!account || account === "null" || account === "undefined")
      return false
    return true
  },
  async fetchAccountData(account, chainId, checkSign) {
    var that = this
    console.log("account", account)
    console.log("chainId", chainId)
    const web3 = this.getWeb3()
    if (!chainId) {
      chainId = await that.getChainId()
      that.chainId = chainId
    } else {
      that.chainId = chainId
    }
    store.commit("user/SET_NETWORK_ID", Number(that.chainId))

    if (!account) {
      const accounts = await web3.eth.getAccounts()
      this.account = accounts[0]
    } else {
      that.account = account
    }
    localStorage.setItem('account', that.account)
    store.commit("user/SET_ACCOUNT", that.account)
    that.addConnectedChange()
  },

  addConnectedChange() {
    const event = new CustomEvent('onConnected', {
      detail: {
        account: this.account,
        chainId: this.chainId,
        signature: this.signature,
        time: new Date(),
      },
      bubbles: true,
      cancelable: true,
    })
    window.document.dispatchEvent(event)
  },

  isUserOnRightChain(chainId) {
    return chainId === 97 || chainId === 80001 || chainId === 1666700000
  },
  // 点击钱包连接
  async walletConnect() {
    await this.initWallet();
    await this.onConnect();
  },

  async disconnect() {
    console.log(this.provider)
    if (this.provider) {
      console.log('walletDisConnect')
      if (this.provider.close) {
        await this.provider.close();
      }
      if (this.provider.disconnect) {
        await this.provider.disconnect();
      }
      this.provider = null
    }
    this.account = null
    store.commit("user/SET_ACCOUNT", null)
    localStorage.removeItem('account')
    const event = new CustomEvent('disconnect', {
      detail: {
        time: new Date(),
      },
      bubbles: true,
      cancelable: true,
    })
    window.document.dispatchEvent(event)

  },
  async getFaucetContract() {
    const web3 = this.getWeb3()
    const contract = new web3.eth.Contract(Faucet, BSCFaucetAddress)
    return contract
  },
  async getDaoTokenContract() {
    const web3 = this.getWeb3()
    const contract = new web3.eth.Contract(DaoToken, DAOTokenAddress)
    return contract
  },
}
