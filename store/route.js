export const state = () => ({
  routeinfo: {
    path: '/',
    fullPath: '/'
  }
})

export const mutations = {
  routeinfo(state, routeinfo) {
    state.routeinfo = routeinfo
  }
}
