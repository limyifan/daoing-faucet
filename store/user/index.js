export default {
    namespaced: true,
    state() {
      return {
        networkId: "",
        account: "",
        email: "",
      }
    },
    getters: {
        networkId: state => state.networkId,
        account: state => state.account,
        email: state => state.email,
    },
    mutations: {
        SET_NETWORK_ID(state, val) {
            state.networkId = val
        },
        SET_ACCOUNT(state, val) {
            state.account = val
        },
        SET_EMAIL(state, val) {
            state.email = val
        },
    },
    actions: {},
}
